package services.booking;

import dao.booking.Booking;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class BookingServiceImplTest {
    private final BookingService bookingService = new BookingServiceImpl();

    @Test
    public void testCreateBooking() {
        bookingService.createBooking(1, "Marta");
        List<String> passengers = new ArrayList<>();
        passengers.add("Marta");
        int idM = bookingService.getBookingsByName("Marta").get(0).id();
        Booking testSave = new Booking(idM, 1, passengers);
        List<Booking> finder = bookingService.getAllBookings();
        int lastId = finder.get(finder.size()-1).id();
        Booking expected = bookingService.getBookingById(lastId);
        bookingService.deleteBooking(lastId);

        assertEquals(testSave, expected);
    }

    @Test
    public void testCountSitsBooked() {
        HashMap<Integer, Booking> bookings = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("bookings.bin"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Booking booking = Booking.deserialize(line);
                bookings.put(booking.id(), booking);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Booking> allBookings = new ArrayList<> (bookings.values());

        int counter = allBookings.stream().filter(b -> b.getFlightId() == 2)
                .mapToInt(Booking::countPassengers)
                .sum();

        int expected = bookingService.getAllBookings().stream()
                .filter(b -> b.getFlightId() == 2)
                .mapToInt(Booking::countPassengers)
                .sum();

        assertEquals(counter, expected);
    }

    @Test
    public void testGetBookingsByName() {
        HashMap<Integer, Booking> bookings = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("bookings.bin"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Booking booking = Booking.deserialize(line);
                bookings.put(booking.id(), booking);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Booking> all = new ArrayList<> (bookings.values());
        List<Booking> flights = all.stream()
                .filter(b -> b.getPassengers().contains("BBB"))
                .collect(Collectors.toList());

        List<Booking> expected = bookingService.getAllBookings().stream()
        .filter(b -> b.getPassengers().contains("BBB"))
        .collect(Collectors.toList());

        assertEquals(flights, expected);
    }
}