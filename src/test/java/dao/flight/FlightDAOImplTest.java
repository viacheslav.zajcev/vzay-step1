package dao.flight;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FlightDAOImplTest {
    IFlightDAO flightDAO = new FlightDAOImpl();

    @Test
    public void find() {
        Flight fetchedFlight = flightDAO.find(1).get();
        String inputString = "2023-11-19T14:55";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime parsedDateTime = LocalDateTime.parse(inputString, formatter);
        Flight expected = new Flight(1, parsedDateTime, "Dublin", 80);

        assertEquals(expected, fetchedFlight);
    }

    @Test
    public void findAll() {
        List<Flight> foundFlights = flightDAO.findAll();
        List<Flight> expected = new ArrayList<>();

        expected.add(new Flight(1, LocalDateTime.parse("2023-11-19T14:55"), "Dublin", 80));
        expected.add(new Flight(2, LocalDateTime.parse("2023-11-11T08:45"), "London", 80));
        expected.add(new Flight(3, LocalDateTime.parse("2023-11-11T20:30"), "London", 100));
        expected.add(new Flight(4, LocalDateTime.parse("2023-11-12T20:15"), "Tokyo", 150));
        expected.add(new Flight(5, LocalDateTime.parse("2023-11-13T12:00"), "Paris", 100));
        expected.add(new Flight(6, LocalDateTime.parse("2023-11-14T06:30"), "Berlin", 60));
        expected.add(new Flight(7, LocalDateTime.parse("2023-11-15T18:45"), "Sydney", 180));
        expected.add(new Flight(8, LocalDateTime.parse("2023-11-16T10:20"), "Bali", 130));
        expected.add(new Flight(9, LocalDateTime.parse("2023-11-17T22:00"), "Rome", 90));
        expected.add(new Flight(10, LocalDateTime.parse("2023-11-18T16:10"), "Dubai", 110));

        assertEquals(expected, foundFlights);
    }

    @Test
    public void save() {
        Flight flight = new Flight(11, LocalDateTime.parse("2023-11-18T16:10"), "Dubai", 110);
        
        flightDAO.save(flight);

        Flight justSavedFlight = flightDAO.find(flight.getId()).get();

        assertEquals(flight, justSavedFlight);

        flightDAO.delete(justSavedFlight.getId());
    }

    @Test
    public void update() {
        Flight flight = flightDAO.find(1).get();

        flight.setDestination("Test");

        flightDAO.update(flight);

        Flight afterUpdate = flightDAO.find(1).get();

        assertEquals(flight, afterUpdate);

        flight.setDestination("Dublin");

        flightDAO.update(flight);
    }

    @Test
    public void delete() {
        List<Flight> allFlights = flightDAO.findAll();

        Flight lastFlight = allFlights.get(allFlights.size() - 1);

        flightDAO.delete(lastFlight.getId());

        List<Flight> flightsAfterDeletion = flightDAO.findAll();

        int expectedNewLength = flightsAfterDeletion.size();
        int initialLength = allFlights.size();

        assertEquals(expectedNewLength, initialLength - 1);

        flightDAO.save(lastFlight);
    }

    @Test
    public void readFlightsFromFile() {
        List<Flight> flights = FlightDAOImpl.readFlightsFromFile();

        flights.add(new Flight(1, LocalDateTime.parse("2023-11-19T14:55"), "Dublin", 80));
        flights.add(new Flight(2, LocalDateTime.parse("2023-11-11T08:45"), "London", 80));
        flights.add(new Flight(3, LocalDateTime.parse("2023-11-11T20:30"), "London", 100));
        flights.add(new Flight(4, LocalDateTime.parse("2023-11-12T20:15"), "Tokyo", 150));
        flights.add(new Flight(5, LocalDateTime.parse("2023-11-13T12:00"), "Paris", 100));
        flights.add(new Flight(6, LocalDateTime.parse("2023-11-14T06:30"), "Berlin", 60));
        flights.add(new Flight(7, LocalDateTime.parse("2023-11-15T18:45"), "Sydney", 180));
        flights.add(new Flight(8, LocalDateTime.parse("2023-11-16T10:20"), "Bali", 130));
        flights.add(new Flight(9, LocalDateTime.parse("2023-11-17T22:00"), "Rome", 90));
        flights.add(new Flight(10, LocalDateTime.parse("2023-11-18T16:10"), "Dubai", 110));

        assertEquals(flights, flights);
    }
}