package dao.booking;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BookingDaoImplTest {
    BookingDAO bookingDAO = new BookingDAOImpl();

    @Test
    public void testSave() {
        Booking testSave = new Booking(1, "Marta");
        List<Booking> finder = bookingDAO.findAll();
        int lastId = finder.get(finder.size()-1).id();
        bookingDAO.save(testSave);
        Booking expected = bookingDAO.find(lastId+1).get();
        bookingDAO.delete(lastId+1);

        assertEquals(testSave, expected);
    }

    @Test
    public void testFind() {
        Booking testFind = bookingDAO.find(1).get();
        List<String> passengers = new ArrayList<>();
        String firstName = testFind.getPassengers().get(0);
        String secondName = testFind.getPassengers().get(1);
        String thirdName = testFind.getPassengers().get(2);
        passengers.add(firstName);
        passengers.add(secondName);
        passengers.add(thirdName);
        Booking expected = new Booking(1, 1, passengers);

        assertEquals(testFind, expected);
    }

    @Test
    public void testFindAll() {
        HashMap<Integer, Booking> bookings = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("bookings.bin"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Booking booking = Booking.deserialize(line);
                bookings.put(booking.id(), booking);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    List<Booking> allBookings = new ArrayList<> (bookings.values());
    List<Booking> expected = bookingDAO.findAll();

    assertEquals(allBookings, expected);
    }

    @Test
    public void testDelete() {

        Booking testSave = new Booking(1, "Marta");
        List<Booking> finder = bookingDAO.findAll();
        int lastId = finder.get(finder.size()-1).id();
        bookingDAO.save(testSave);

        HashMap<Integer, Booking> bookings = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("bookings.bin"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Booking booking = Booking.deserialize(line);
                bookings.put(booking.id(), booking);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        bookings.remove(lastId+1);
        List<Booking> savedList = new ArrayList<> (bookings.values());

        bookingDAO.delete(lastId+1);
        List<Booking> expected = bookingDAO.findAll();

        assertEquals(savedList, expected);
    }

}
