import controllers.booking.BookingContorllerImpl;
import controllers.booking.BookingController;
import controllers.flight.FlightControllerImpl;
import controllers.flight.IFlightController;
import dao.booking.Booking;
import dao.flight.Flight;
import exceptions.InvalidInputException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConsoleApp {
    final static IFlightController flightController = new FlightControllerImpl();
    final static BookingController bookingController = new BookingContorllerImpl();
    final static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        sc.useDelimiter(System.getProperty("line.separator"));
        while (true) {
            try {
                showMainMenu();
                switch (sc.next()) {
                    case "1":
                        showFlightBoard();
                        break;
                    case "2":
                        showFlightDetails();
                        break;
                    case "3":
                        createBooking();
                        break;
                    case "4":
                        showUserBookings();
                        break;
                    case "5":
                        cancelBooking();
                        break;
                    case "6":
                        exit();
                        break;
                    default:
                        throw new InvalidInputException("Choose the existing menu item.");
                }
            } catch (InvalidInputException e) {
                System.err.println(e.getMessage());
            }
        }

    }

    private static void showMainMenu() {
        String sb = "\n------------------------------\n" +
                "\u001B[32mWELCOME TO FLIGHTS BOOKING\u001B[0m\n\n" +
                "1 - Show flight board\n" +
                "2 - Show flight details\n" +
                "3 - Booking\n" +
                "4 - Show user bookings\n" +
                "5 - Cancel booking\n" +
                "6 - Exit\n";
        System.out.println(sb);
        System.out.print("> ");
    }

    private static void showFlightBoard() {
        List<Flight> allFlights = flightController.getAllFlights();
        allFlights.forEach(flight -> System.out.printf("ID: %d | Kyiv - %s | %s\n",
                flight.getId(),
                flight.getDestination(),
                flight.getFormattedDT()));
    }

    private static void showFlightDetails() throws InvalidInputException, NoSuchElementException {
        System.out.print("Enter the flight id: ");
        try {
            int flightId = Integer.parseInt(sc.next());
            Flight flight = flightController.getFlightById(flightId);
            int freeSeats = flight.getCapacity() - bookingController.countSitsBooked(flightId);
            String flightDetailsTemplate = "ID: %d\n" +
                    "Date/Time: %s\n" +
                    "Departure: Kyiv\n" +
                    "Destination: %s\n" +
                    "Capacity: %d\n" +
                    "Free seats: %d\n";
            System.out.printf(flightDetailsTemplate,
                    flight.getId(),
                    flight.getFormattedDT(),
                    flight.getDestination(),
                    flight.getCapacity(),
                    freeSeats);
        } catch (NoSuchElementException e) {
            throw new InvalidInputException("No flight found.");
        } catch (NumberFormatException e) {
            throw new InvalidInputException("Bad id input.");
        }
    }

    private static void createBooking() {
        try {
            System.out.print("Enter destination: ");
            String destination = sc.next();
            System.out.print("Enter date (yyyy-MM-dd): ");
            String date = sc.next();
            System.out.print("Enter number of passengers: ");
            int numberOfPassengers = Integer.parseInt(sc.next());

            List<Flight> availableFlights = flightController.searchFlightsByCriteria(destination, date, numberOfPassengers);
            if (availableFlights.isEmpty()) {
                System.out.println("No flights matched.");
                return;
            }
            System.out.println("Please choose a flight from the list.");

            availableFlights.forEach(flight -> System.out.printf("ID: %d | Kyiv - %s | %s\n",
                    flight.getId(),
                    flight.getDestination(),
                    flight.getFormattedDT()));

            System.out.print("Enter flight ID or 0 to exit: ");
            int flightToBookId = Integer.parseInt(sc.next());
            if (flightToBookId == 0) return;
            List<String> passengers = IntStream.range(0, numberOfPassengers)
                    .mapToObj(i -> {
                        System.out.printf("Passenger %d: ", i + 1);
                        return sc.next();
                    })
                    .collect(Collectors.toList());

            bookingController.createBooking(flightToBookId, String.valueOf(passengers));

            System.out.println("\u001B[32mBooked successfully.\u001B[0m");
        } catch (Exception e) {
            System.out.println("\u001B[31mInvalid input.\u001B[0m");
        }
    }

    private static void showUserBookings() {
        List<Booking> allBookings = bookingController.getAllBookings();
        allBookings.forEach(booking -> {
            Flight flight = flightController.getFlightById(booking.getFlightId());
            String passengersToShow = String.join(", ", booking.getPassengers());
            String bookingToShow = String.format("ID: %d | Kyiv-%s | %s | Passenger: %s",
                    booking.id(),
                    flight.getDestination(),
                    flight.getFormattedDT(),
                    passengersToShow);

            System.out.println(bookingToShow);
        });
    }

    private static void cancelBooking() throws InvalidInputException {
        System.out.print("Enter booking id: ");
        try {
            int bookingId = Integer.parseInt(sc.next());
            bookingController.deleteBooking(bookingId);
        } catch (NoSuchElementException e) {
            throw new InvalidInputException("No flight found.");
        } catch (NumberFormatException e) {
            throw new InvalidInputException("Bad id input.");
        }
    }

    private static void exit() {
        System.out.println("\u001B[32mGOOD BYE!\u001B[0m");
        System.exit(0);
    }
}
