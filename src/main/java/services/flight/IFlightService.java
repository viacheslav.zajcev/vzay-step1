package services.flight;

import dao.flight.Flight;

import java.util.List;

public interface IFlightService {
    Flight getFlightById(int id);

    List<Flight> getAllFlights();

    List<Flight> searchFlightsByCriteria(String destination, String date, int numberOfPassengers);

    void updateFlight(Flight flight);
}
