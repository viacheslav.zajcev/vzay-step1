package services.flight;

import dao.flight.Flight;
import dao.flight.IFlightDAO;
import dao.flight.FlightDAOImpl;
import services.booking.BookingService;
import services.booking.BookingServiceImpl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class FlightServiceImpl implements IFlightService {
    private final IFlightDAO flightDAO = new FlightDAOImpl();
    private final BookingService bookingService = new BookingServiceImpl();
    @Override
    public Flight getFlightById(int id) {
        return flightDAO.find(id).get();
    }

    @Override
    public List<Flight> getAllFlights() {
        return flightDAO.findAll();
    }

    @Override
    public List<Flight> searchFlightsByCriteria(String destination, String date, int numberOfPassengers) {
        return flightDAO.findAll().stream()
                .filter(flight -> flight.getDestination().equals(destination)
                        && flight.getDateTime().toLocalDate().equals(LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                        && numberOfPassengers <= flight.getCapacity() - bookingService.countSitsBooked(flight.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public void updateFlight(Flight flight) {
        flightDAO.update(flight);
    }
}
