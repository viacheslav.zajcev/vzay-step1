package services.booking;

import dao.booking.Booking;
import dao.flight.Flight;

import java.util.List;

public interface BookingService {

    Booking getBookingById(int id);

    List<Booking> getAllBookings();

    List<Booking> getBookingsByName(String name);

    void createBooking(int flightId, String... passengers);

    int countSitsBooked(int flightId);

    void deleteBooking(int id);

}
