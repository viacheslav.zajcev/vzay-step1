package services.booking;


import dao.booking.Booking;
import dao.booking.BookingDAO;
import dao.booking.BookingDAOImpl;
import dao.flight.Flight;
import dao.flight.FlightDAOImpl;
import dao.flight.IFlightDAO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class BookingServiceImpl implements BookingService {

    private final BookingDAO bookingDAO = new BookingDAOImpl();

    @Override
    public Booking getBookingById(int id) {
        return bookingDAO.find(id).get();
    }

    @Override
    public List<Booking> getAllBookings() {
        return bookingDAO.findAll();
    }

    @Override
    public void createBooking(int flightId, String... passengers) {
        Booking b = new Booking(flightId, passengers);
        bookingDAO.save(b);
    }

    @Override
    public int countSitsBooked(int flightId) {
        return bookingDAO.findAll().stream()
                .filter(b -> b.getFlightId() == flightId)
                .mapToInt(Booking::countPassengers)
                .sum();
        }

    @Override
    public List<Booking> getBookingsByName(String name){
        return bookingDAO.findAll().stream()
                .filter(b -> b.getPassengers().contains(name))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteBooking(int id) {
        bookingDAO.delete(id);
    };
}
