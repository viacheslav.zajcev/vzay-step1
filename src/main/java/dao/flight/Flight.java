package dao.flight;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Flight implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private LocalDateTime dateTime;
    private String destination;
    private Integer capacity;

    public Flight() {
    }

    public Flight(int id, LocalDateTime dateTime, String destination, Integer capacity) {
        this.id = id;
        this.dateTime = dateTime;
        this.destination = destination;
        this.capacity = capacity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getFormattedDT() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return this.getDateTime().format(formatter);
    }

    @Override
    public String toString() {
        return String.format("Flight{id=%d, dateTime=%s, destination='%s', capacity=%d}", id, dateTime, destination, capacity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return getId() == flight.getId() && Objects.equals(getDateTime(), flight.getDateTime()) && Objects.equals(getDestination(), flight.getDestination()) && Objects.equals(getCapacity(), flight.getCapacity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDateTime(), getDestination(), getCapacity());
    }

    public String serialize() {
        return this.toString();
    }

    public static Flight deserialize(String serializedFlight) {
        String[] parts = serializedFlight.split(", ");
        int id = Integer.parseInt(parts[0].substring(parts[0].indexOf('=') + 1));
        LocalDateTime dateTime = LocalDateTime.parse(parts[1].substring(parts[1].indexOf('=') + 1));
        String destination = parts[2].substring(parts[2].indexOf('=') + 2, parts[2].length() - 1);
        int capacity = Integer.parseInt(parts[3].substring(parts[3].indexOf('=') + 1, parts[3].length() - 1));

        return new Flight(id, dateTime, destination, capacity);
    }
}
