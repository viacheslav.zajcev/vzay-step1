package dao.flight;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FlightDAOImpl implements IFlightDAO {
    @Override
    public Optional<Flight> find(long id) {
        return readFlightsFromFile().stream()
                .filter(flight -> flight.getId() == id)
                .findFirst();
    }

    @Override
    public List<Flight> findAll() {
        return readFlightsFromFile();
    }

    @Override
    public void save(Flight flight) {
        List<Flight> flights = readFlightsFromFile();

        int newId = flights.stream().max(Comparator.comparing(Flight::getId)).get().getId() + 1;

        flight.setId(newId);
        flights.add(flight);

        String toPersist = flights.stream()
                .map(Flight::serialize)
                .collect(Collectors.joining("\n"));

        try (PrintWriter writer = new PrintWriter(new FileWriter("flights.bin"))) {
            writer.write(toPersist);
        } catch (IOException e) {
            throw new RuntimeException("Error while saving flight to file", e);
        }
    }

    @Override
    public void update(Flight flight) {
        List<Flight> flights = readFlightsFromFile();
        flights.set(flight.getId() - 1, flight);

        String toPersist = flights.stream()
                .map(Flight::serialize)
                .collect(Collectors.joining("\n"));

        try (PrintWriter writer = new PrintWriter(new FileWriter("flights.bin"))) {
            writer.write(toPersist);
        } catch (IOException e) {
            throw new RuntimeException("Error while updating flight in file", e);
        }
    }

    @Override
    public void delete(int id) {
        List<Flight> flights = readFlightsFromFile();
        flights.remove(id - 1);

        String toPersist = flights.stream()
                .map(Flight::serialize)
                .collect(Collectors.joining("\n"));

        try (PrintWriter writer = new PrintWriter(new FileWriter("flights.bin"))) {
            writer.write(toPersist);
        } catch (IOException e) {
            throw new RuntimeException("Error while deleting flight from file", e);
        }
    }

    public static List<Flight> readFlightsFromFile() {
        List<Flight> flights = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("flights.bin"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Flight flight = Flight.deserialize(line);
                flights.add(flight);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return flights;
    }
}
