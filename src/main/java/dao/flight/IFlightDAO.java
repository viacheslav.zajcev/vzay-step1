package dao.flight;

import java.util.List;
import java.util.Optional;

public interface IFlightDAO {
    Optional<Flight> find(long id);

    List<Flight> findAll();

    void save(Flight flight);

    void update(Flight flight);

    void delete(int id);
}
