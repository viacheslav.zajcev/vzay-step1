package dao.booking;

public enum Destination {
    DUBLIN
            {
        @Override
        public String toString() {
            return "Dublin";
        }
    },
    AMSTERDAM
            {
        @Override
        public String toString() {
            return "Amsterdam";
        }
    },
    LONDON
            {
        @Override
        public String toString() {
            return "London";
        }
    },
    TOKYO
            {
        @Override
        public String toString() {
            return "Tokyo";
        }
    },
    SYDNEY
            {
        @Override
        public String toString() {
            return "Sydney";
        }
    },
    BALI
            {
        @Override
        public String toString() {
            return "Bali";
        }
    },
    ROME
            {
        @Override
        public String toString() {
            return "Rome";
        }
    },
    DUBAI
            {
        @Override
        public String toString() {
            return "Dubai";
        }
    },
    PARIS
            {
        @Override
        public String toString() {
            return "Paris";
        }
    },
    BERLIN
            {
        @Override
        public String toString() {
            return "Berlin";
        }
    },

}
