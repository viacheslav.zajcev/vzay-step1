package dao.booking;

import java.io.*;
import java.util.*;

import static java.util.stream.Collectors.joining;

public class BookingDAOImpl implements BookingDAO {

    @Override
    public void save(Booking b) {
        HashMap<Integer, Booking> bookings = readBookingsFromFile();

        if (bookings.isEmpty()) {
        b.setId(1);
        }
        else b.setId(bookings.entrySet().stream().max(Map.Entry.comparingByKey()).get().getKey() + 1);
        bookings.put(b.id(), b);

        String toPersist = bookings.values().stream()
                .map(Booking::serialize)
                .collect(joining("\n"));

        try (PrintWriter writer = new PrintWriter(new FileWriter("bookings.bin"))) {
            writer.write(toPersist);
        } catch (IOException e) {
            throw new RuntimeException("Error while saving booking to file", e);
        }
    }

    @Override
    public Optional<Booking> find(int id) {
        Booking b = readBookingsFromFile().get(id);
        return Optional.ofNullable(b);
    }

    @Override
    public List<Booking> findAll() {
        return new ArrayList<>(readBookingsFromFile().values());
    }

    @Override
    public void delete(int id) {
        HashMap<Integer, Booking> bookings =  readBookingsFromFile();
        bookings.remove(id);

        String toPersist = bookings.values().stream()
                .map(Booking::serialize)
                .collect(joining("\n"));

        try (PrintWriter writer = new PrintWriter(new FileWriter("bookings.bin"))) {
            writer.write(toPersist);
        } catch (IOException e) {
            throw new RuntimeException("Error while deleting booking from file", e);
        }
    }

    public static HashMap<Integer, Booking> readBookingsFromFile() {
        HashMap<Integer, Booking> bookings = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("bookings.bin"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Booking booking = Booking.deserialize(line);
                bookings.put(booking.id(), booking);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookings;
    }
}

