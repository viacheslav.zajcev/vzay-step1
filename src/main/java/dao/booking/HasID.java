package dao.booking;

public interface HasID {
    int id();
}