package dao.booking;

import java.io.Serializable;
import java.util.*;

public class Booking implements Serializable, HasID {
    private int id;
    private int flightId;
    private List<String> passengers;

    public Booking(int id,  int flightId, List<String> passengers) {
    this.id = id;
    this.flightId = flightId;
    this.passengers = passengers;
    }
    private static List<String> combinePassengers(String... passengers) {
        return new ArrayList<>(Arrays.asList(passengers));
    }

    public Booking(int flightId, String... passengers) {
        this.flightId = flightId;
        this.passengers = combinePassengers(passengers);
    }

    @Override
    public int id() {return this.id;}

    public int countPassengers () {
        return passengers.size();
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<String> passengers) {
        this.passengers = passengers;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    @Override
    public String toString() {
        String passengersStr = passengers.toString()
                .replace("]", "")
                .replace("[", "")
                .replace(",", "_");

        return String.format(
                "Booking{id=%d, flightId=%d, passengers=%s}",
                id, flightId, passengersStr);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return id == booking.id && flightId == booking.flightId && Objects.equals(passengers, booking.passengers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, flightId, passengers);
    }

    public String serialize() {
        return this.toString();
    }

    public static Booking deserialize(String serializedBooking) {
        String[] parts = serializedBooking.split(", ");
        int id = Integer.parseInt(parts[0].substring(parts[0].indexOf('=') + 1));
        int flightId = Integer.parseInt(parts[1].substring(parts[1].indexOf('=') + 1));
        String passenger =  parts[2].substring(parts[2].indexOf('=') + 1, parts[2].length() - 1);
        List<String> passengers = new ArrayList<> (Arrays.asList(passenger.split("_ ")));

        return new Booking(id, flightId, passengers);
    }
}

