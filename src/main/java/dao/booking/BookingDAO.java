package dao.booking;

import java.util.List;
import java.util.Optional;

public interface BookingDAO {

        List<Booking> findAll();

        void save(Booking booking);

        Optional<Booking> find(int id);

        void delete(int id);

}
