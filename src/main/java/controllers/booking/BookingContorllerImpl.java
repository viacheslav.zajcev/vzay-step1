package controllers.booking;

import dao.booking.Booking;
import services.booking.BookingService;
import services.booking.BookingServiceImpl;

import java.util.List;

public class BookingContorllerImpl implements BookingController{
    private final BookingService bookingService = new BookingServiceImpl();

    @Override
    public Booking getBookingById(int id) {
        return bookingService.getBookingById(id);
    }

    @Override
    public List<Booking> getAllBookings(){
       return bookingService.getAllBookings();
    }

    @Override
    public List<Booking> getBookingsByName(String name){
    return bookingService.getBookingsByName(name);
    }

    @Override
    public void createBooking(int flightId, String... passengers){
        bookingService.createBooking(flightId, passengers);
    }

    @Override
    public int countSitsBooked(int flightId){
       return bookingService.countSitsBooked(flightId);
    }

    @Override
    public void deleteBooking(int id){
        bookingService.deleteBooking(id);
    }

}
