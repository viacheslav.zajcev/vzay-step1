package controllers.flight;

import dao.flight.Flight;

import java.util.List;

public interface IFlightController {
    List<Flight> getAllFlights();

    Flight getFlightById(int id);

    List<Flight> searchFlightsByCriteria(String destination, String date, int numberOfPassengers);
}
