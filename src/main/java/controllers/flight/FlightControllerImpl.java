package controllers.flight;

import dao.flight.Flight;
import services.flight.FlightServiceImpl;
import services.flight.IFlightService;

import java.util.List;

public class FlightControllerImpl implements IFlightController {
    private final IFlightService flightService = new FlightServiceImpl();

    @Override
    public List<Flight> getAllFlights() {
        return flightService.getAllFlights();
    }

    @Override
    public Flight getFlightById(int id) {
        return flightService.getFlightById(id);
    }

    @Override
    public List<Flight> searchFlightsByCriteria(String destination, String date, int numberOfPassengers) {
        return flightService.searchFlightsByCriteria(destination, date, numberOfPassengers);
    }
}
